import os,sys
import json
import time
import calendar
import datetime
# import tornado.httpserver
from tornado import ioloop,web
import urllib
import redis,ast
import hashlib
from memsql.common import database
import MySQLdb
import ast
import operator
import os, uuid
import numpy
from pymongo import MongoClient
from bson import ObjectId
import dicom
from skimage.io import imsave
import sys
patient_data = MongoClient("mongodev.paralleldots.com")["patient"]["patient_doc"]

__UPLOADS__ = "static/uploads/"

class PicsUploadHandler(web.RequestHandler):
    def post(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        
        fileinfo        = self.request.files['file1'][0]
        doctor_id       = self.get_body_argument("doc_id") 
        fname           = fileinfo['filename']
        fname1          = fname                
        #print fname1
        check_for_fname = patient_data.find_one({'doctor_id':doctor_id,'local_image_name':fname1})
        
        if check_for_fname is not None:
            self.set_header("Content-Type", "application/json")
            self.set_status(200)
            self.finish(json.dumps({'Status':0,'msg':"Image with this name already uploaded"}))
            return

        # if extn == '.dcm':
        with open(__UPLOADS__ + fname1, 'wb') as fh:
            fh.write(bytes(fileinfo['body']))

        try:
            dataset = dicom.read_file(__UPLOADS__ + fname1)
            if "PatientBirthDate" in dataset:
                dob    = dataset.PatientBirthDate
                if dob != '':
                    dob = dob[:4]
                else:
                    dob = "" 
            else:
                dob = ""
            
            if "PatientSex" in dataset:
                gender = dataset.PatientSex 
                if gender == "M":
                    gender = "Male"
                else:
                    gender = "Female" 
            else:
                gender = ""   

            if "PatientID" in dataset:
                fname     = dataset.PatientID
            else:
                fname = "KG"
            
            now       = datetime.date.today()
            startdate = now.strftime("%d%B%Y")
            cname     = str(fname)+"_tagged_"+str(startdate)+"_bwxKG"

            dataset1 = dataset.pixel_array
            dataset1 = dataset1/float(dataset1.max())
            imsave(__UPLOADS__+str(cname) + '.jpg',dataset1)
            
            self.set_header("Content-Type", "application/json")
            self.set_status(200)
            self.finish(json.dumps({'Status':1, 'created_img_name':str(cname)+".jpg", 'local_image_name':fname1, 'dob':dob, 'gender':gender, 'image':"http://utilities.paralleldots.com/diacom/"+str(cname)+".jpg"}))
            return
        except Exception, e:
            print e
            final_name = fname 
            fname      = fname.split(".")
            fname      = fname[0]
            now        = datetime.date.today()
            startdate  = now.strftime("%d%B%Y")
            cname      = str(fname)+"_tagged_"+str(startdate)+"_bwxKG.jpg"
            with open(__UPLOADS__ + cname, 'wb') as fh:
                fh.write(bytes(fileinfo['body']))
            self.set_header("Content-Type", "application/json")
            self.set_status(200)
            self.finish(json.dumps({'Status':1, 'created_img_name':cname, 'local_image_name':final_name, 'dob':"", 'gender':"", 'image':"http://utilities.paralleldots.com/diacom/"+str(cname)}))
            return
            
class ServeImagesHandler(web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        doc_id = self.get_argument("doc_id")
        data   = patient_data.find({'doctor_id':doc_id,'save_as_draft':0})
        drafts = []
        for obj in data:
            drafts.append({'dob':obj.get('dob'),'gender':obj.get('gender'),'ethnicity':obj.get('ethnicity'),'diabetes':obj.get('diabetes'),'smoking':obj.get('smoking'),'other_info':obj.get('other_info'),'reflux':obj.get('reflux'),'dry_mouth':obj.get('dry_mouth'),'sealant':obj.get('sealant'),'image_name':obj.get('local_image_name'),'created_at':str(obj.get('created_at')),'pid':str(obj.get('_id')),'mask':obj.get('mask'),'doctor_id':obj.get('doctor_id'),'image_link':"http://utilities.paralleldots.com/diacom/"+str(obj.get('image'))})
        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        self.finish(json.dumps({'Status':1,'drafts':drafts}))
        return

class UpdatePatientHandler(web.RequestHandler):
    def post(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        p_data           = json.loads(urllib.unquote_plus(self.request.body)) 
        
        pid              = p_data.get('pid',None)
        dob              = p_data.get('dob',None)
        gender           = p_data.get('gender',None)
        ethnicity        = p_data.get('ethnicity',None)
        diabetes         = p_data.get('diabetes',None)
        smoking          = p_data.get('smoking',None)
        other_info       = p_data.get('other_info',None)
        reflux           = p_data.get("reflux",None)
        dry_mouth        = p_data.get('dry_mouth',None)
        sealant          = p_data.get('sealant',None)
        update      = patient_data.update({'_id': ObjectId(str(pid))}, {'$set': {'reflux':reflux,'dry_mouth':dry_mouth,'sealant':sealant,'dob':dob,'gender':gender,'ethnicity':ethnicity,'diabetes':diabetes,'smoking':smoking,'other_info':other_info} })
        
        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        self.finish(json.dumps({'Status':1}))
        return


class SaveDraftHandler(web.RequestHandler):
    def post(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        mask_data  = json.loads(urllib.unquote_plus(self.request.body)) 
        pid        = mask_data.get('pid',None)
        mask       = mask_data.get('mask',None)
        update     = patient_data.update({'_id': ObjectId(str(pid))}, {'$set': {'mask':json.dumps(json.loads(mask))} })
        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        self.finish(json.dumps({'Status':1}))
        return


class PatientDeleteHandler(web.RequestHandler):
    def post(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token') 
        pid        = self.get_argument('pid',None)
        
        if pid is None:
            self.set_header("Content-Type", "application/json")
            self.set_status(200)
            self.finish(json.dumps({'Status':0,'msg':"id is required"}))
            return

        delete     = patient_data.remove({'_id': ObjectId(str(pid))})
        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        self.finish(json.dumps({'Status':1,'msg':'deleted'}))
        return


class PatientImageInfo(web.RequestHandler):
    def post(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        
        # p_info_data        = json.loads(urllib.unquote_plus(self.request.body)) 
        dob                = self.get_body_argument("dob")
        gender             = self.get_body_argument("gender")
        ethnicity          = self.get_body_argument("ethnicity")
        diabetes           = self.get_body_argument("diabetes")
        smoking            = self.get_body_argument("smoking")
        other_info         = self.get_body_argument("other_info")
        doctor_id          = self.get_body_argument("doc_id")
        local_image_name   = self.get_body_argument('local_image_name',None)
        created_image_name = self.get_body_argument('created_image_name',None)
        reflux             = self.get_body_argument("reflux",None)
        dry_mouth          = self.get_body_argument('dry_mouth',None)
        sealant            = self.get_body_argument('sealant',None)
        insert = patient_data.insert({'reflux':reflux,'dry_mouth':dry_mouth,'sealant':sealant,'created_at':datetime.datetime.now(),'local_image_name':local_image_name,'dob':dob,'gender':gender,'ethnicity':ethnicity,'diabetes':diabetes,'smoking':smoking,'other_info':other_info,'image':created_image_name,'doctor_id':doctor_id,'save_as_draft':0,'mask':{}})
        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        self.finish(json.dumps({'Status':1,'pid':str(insert),'image':"http://utilities.paralleldots.com/diacom/"+str(created_image_name)}))
        return



class MaskTypeCountHandler(web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        
        tagname = ['Caries - Detected - Depth Unknown','Caries - Detected - Enamel','Caries - Detected - Dentin','Caries - Detected - Pulp','Caries - Detected - Recurrent','Caries - Undetected - Got Missed','Caries - Undetected - Not Visible','Pocket','Bone Loss - low','Bone Loss - moderate','Bone Loss - severe','Lesion','Other Feature']
        print len(tagname) 
        doc_id     = self.get_argument("doc_id",None)
        start_date = self.get_argument("currentdate",None)
        end_date   = self.get_argument("enddate",None)

        if end_date is None:
            start_date = datetime.datetime.strptime(str(start_date),"%Y-%m-%d")
            end_date   = start_date + datetime.timedelta(hours=23, minutes=59)
            temp = []
            data = []
            temp_data   = patient_data.find({'doctor_id':doc_id,'created_at':{'$gte':start_date,"$lte":end_date}})
            for ob in temp_data:
                data.append(ob)
            
            for i in range(0,len(tagname)):

                count = 0
                for obj in data:
                    mask =  json.loads(obj.get('mask'))
                    if len(mask) == 0:
                        continue
                    else:
                        polygondata = mask.get('polygonDatas')

                        for k in range(0,len(polygondata)):
                            if i == 11 or i == 12:
                                if str(tagname[i]) == str(polygondata[k].get('tagname')).split('-')[0].strip():
                                    count = count + 1
                                else:
                                    continue
                            else:
                                if str(tagname[i]) == str(polygondata[k].get('tagname')):
                                    count = count + 1
                                else:
                                    continue

                temp.append({str(tagname[i]):count})
            self.set_header("Content-Type", "application/json")
            self.set_status(200)
            self.finish(json.dumps({'Status':1,'results':temp}))
            return
        else:
            start_date = datetime.datetime.strptime(str(start_date),"%Y-%m-%d")
            end_date   = datetime.datetime.strptime(str(end_date),"%Y-%m-%d")
            end_date   = end_date + datetime.timedelta(hours=23, minutes=59)
            temp = []
            data = []
            temp_data   = patient_data.find({'doctor_id':doc_id,'created_at':{'$gte':start_date,'$lte':end_date}})
            for ob in temp_data:
                data.append(ob)
            
            for i in range(0,len(tagname)):
                count = 0
                for obj in data:
                    mask =  json.loads(obj.get('mask'))
                    if len(mask) == 0:
                        continue
                    else:
                        polygondata = mask.get('polygonDatas')

                        for k in range(0,len(polygondata)):
                            if i == 11 or i == 12:
                                if str(tagname[i]) == str(polygondata[k].get('tagname')).split('-')[0].strip():
                                    count = count + 1
                                else:
                                    continue
                            else:
                                if str(tagname[i]) == str(polygondata[k].get('tagname')):
                                    count = count + 1
                                else:
                                    continue    
                        
                temp.append({str(tagname[i]):count})
            self.set_header("Content-Type", "application/json")
            self.set_status(200)
            self.finish(json.dumps({'Status':1,'results':temp}))
            return    

settings = {
    "template_path": os.path.join(os.path.dirname(__file__), "templates"),
    "static_path": os.path.join(os.path.dirname(__file__), "static"),
    "debug" : False
}

application = web.Application([
    (r'/diacom/api/images/uploads', PicsUploadHandler),
    (r'/diacom/api/serve/images', ServeImagesHandler),
    (r'/diacom/api/save/draft', SaveDraftHandler),
    (r'/diacom/api/update', UpdatePatientHandler),
    (r'/diacom/api/info', PatientImageInfo),
    (r'/diacom/api/draft/delete', PatientDeleteHandler),
    (r'/diacom/api/mask/type/count', MaskTypeCountHandler),
],**settings)

if __name__ == "__main__":
    print "Here we go"
    application.listen(8896)
    ioloop.IOLoop.instance().start()
